class Menu():
    
    def __init__(self):
        self.food={}
        
    def __setitem__(self, key,price):
        self.food[key]=price
        return self
        
    def __str__(self):
        return str(self.food)
        
def main():
    
    m=Menu()    
    m["idly"]=10
    m["vada"]=20
    
    print("type of object - ",type(m))  # <class '__main__.Menu'>
    print(m)

    
main()