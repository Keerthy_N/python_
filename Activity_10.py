#Activity 10 
#Use is a relationship

class Menu(dict): 
   pass

class order(dict):
    def __init__(self, menu):
        super().__init__()

def ItemNotFound(item):
    print(f"Item {item} is not in Menu :(")
    
def b(Menu,order):
    
    bill={}
    for(key,val) in order.items():
        if(key in Menu):
            bill[key]=(order[key]*Menu[key])
        else:
            ItemNotFound(key)
    return bill

def main():
    
    m=Menu()   
    m["idly"]=10
    m["vada"]=20   
    o=order(m)
    o["vada"] = 2
    o["pongal"] = 2 
    o["rice"]=1
    bill=b(m,o)
    print(bill)
    
main()