#Activity 3
def input_two_numbers():
    a,b=input("Input :").split(' ')
    a=int(a)
    b=int(b)
    return a,b

def add(a,b):
    return a+b

def output(a,b,s):
    print("Output : {} + {} is :{}".format(a,b,s))

def main():
    a,b=input_two_numbers()
    sum_=add(a,b)
    output(a,b,sum_)
    
main() 