def get_cs():
     s=input("Input : ")
     return s

def cs_to_lot(cs):

    #using list comprehension
    lot=[(tuple(i.split("="))) for i in cs.split(";")]
    return lot

def lot_to_cs(lot):

    cs=';'.join(['='.join(i) for i in lot])
    return cs

def main():
        
    cs=get_cs()
    lot=cs_to_lot(cs)
    print(f"\ncs_to_lot :{lot}")
    cs=lot_to_cs(lot)
    print(f'\nlot_to_cs:{cs}')
    
main()