#Activity 8 Operator overloading
class Menu:
    def __init__(self):
        self.food={}
        
    def __add__(self,item): #item has been passed as a tuple , not separately
        
        self.food[item[0]]=item[1]
        
        return self
    
    def __str__(self):
        return str(self.food)
         
def main():
    m = Menu()
    m = m + ("idly",10)  +('vada',20)
    print(m)
    
main()
