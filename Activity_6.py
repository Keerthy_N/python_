def get_cs():
     s=input("Input : ")
     return s

def cs_to_dict(ld): #convert string to dictionary   

    d={i[0]:i[1] for i in [tuple(i.split("=")) for i in ld.split(";")]}
    return d


def dict_to_cs(d):
     
    cs=';'.join(['='.join(i) for i in d.items()])
    
    return cs
    
def main():
    cs=get_cs()
    d=cs_to_dict(cs) #convert string to dictionary
    print(f'\nString to dictionary :\n{d}')
    cs=dict_to_cs(d)
    print(f'\nDictionary to string :\n{cs}')
    
main()   