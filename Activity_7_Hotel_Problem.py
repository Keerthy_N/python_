#Activity 7 - classes- Hotel Problem

class menu:
    def __init__(self):
        self.food={}
        
    def add(self, item, price): #concatenation function
        self.item= item
        self.price= price
        self.food[item]= price
        
    def show(self): #to sprint final string
        for item, price in self.food.items():
            print(f'{item} {price}')

    def get_price(self,item):
        return self.food[item]
           
            
def main():
    m=menu()
    m.add("idly",10)
    m.add("vada",20)
    m.show()
    item="idly"
    price=m.get_price(item)
    print(f'The price of {item} is {price}')
      
main()